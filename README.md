# TimeLine Framework (EN) #

TimeLine is a micro-framework PHP that was written for small project development. It's easy to use and require nearly zero configuration to work. TimeLine is designed for beginners who want small base to start their projects.

TimeLine is based on CodeIgniter Framework.

### Features ###

* MVC architecture
* Nearly zero configuration
* Base routing system
* Support custom library and helper
* Localization
* Hooks
* Form verification
* Widget
* Raw output
* View loader
* PDO support
* Twig support
* Configuration manager
* Session manager
* Upload manager

#### Soon ####

* ORM support (not know which)
* More efficient auth library
* Ressources manager (for JS and CSS loading)
* Custom routing

### How to install ? ###

* Clone repository : git clone https://bitbucket.org/<your_name>/timeline.git
* Get submodule (twig) : git submodule init and git submodule update
* It's done

### What are Requirements ? ###

* **PHP 5.3+**
* **Apache 2.2+ (or nginx/lighthttd)**
* PDO

### Are there a documentation ? ###

There no official documentation now.

# TimeLine Framework (FR) #

TimeLine est un micro-framework PHP qui a été écrit pour le développement de petit projet. Il est facile d'utilisation et requière peu de configuration pour fonctionner. TimeLine est pensé pour les débutants qui cherche une petite base pour commencer leurs projets web.

TimeLine est basé sur le framework CodeIgniter.

### Caractéristiques ###

* Architecture MVC
* Presque aucune configuration
* Système basique de routage
* Support des librairies et helper personnalisés
* Localisation
* Hooks
* Vérification des formulaires
* Widget
* Gestion du flux de sortie
* Système de chargement de vue
* Support de PDO
* Support de Twig
* Gestionnaire de configuration
* Gestionnaire de session
* Gestionnaire d'envoie de fichier

#### Bientôt ####

* Support d'un ORM (lequel ?)
* Librairie auth plus efficace
* Gestionnaire de ressources (pour le chargement du JS et du CSS)
* Routes personnalisées

### Comment installer ? ###

* Cloner le répertoire : git clone https://bitbucket.org/<your_name>/timeline.git
* Récuperer les sous-modules (twig) : git submodule init et git submodule update
* C'est terminé

### Qu'elle sont les exigences requises ? ###

* **PHP 5.3+**
* **Apache 2.2+ (ou nginx/lighthttd)**
* PDO

### Il y a t'il une documentation ? ###

Il n'y a aucune documentation officiel pour le moment.